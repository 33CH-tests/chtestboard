# dsPIC33CH128MP505 Test Board

This is a little, hand-wired board to get familiar with the 
dual-core dsPICs. Initially it was just the dsPIC and a few
LEDs, but it got hard to tell if I was actually communicating
between the cores with just LEDs, so I added a connector for
my serial graphics terminal.

Then I got an ADPS-9960 gesture sensor, and this is a 3V3 part,
so onward to adding that to play with.


![Schematic](images/Test_Board_01.png)

--

![Proto](images/33CH-board-avatar.jpg)
